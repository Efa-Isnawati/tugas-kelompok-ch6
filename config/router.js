const express = require("express");
const controllers = require("../app/controllers");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */
// API authentication 
apiRouter.post("/api/v1/register", controllers.api.v1.authController.register);
apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);

// API Users Table
apiRouter.get("/api/v1/user", controllers.api.v1.userController.list);
apiRouter.post("/api/v1/user", controllers.api.v1.userController.create);
apiRouter.put("/api/v1/user/:id", controllers.api.v1.userController.update);
apiRouter.get("/api/v1/user/:id", controllers.api.v1.userController.show);
apiRouter.get("/api/v1/users", controllers.api.v1.userController.findAlllist);
apiRouter.delete(
  "/api/v1/user/:id",
  controllers.api.v1.userController.destroy
);

// API Tikets Table
apiRouter.get("/api/v1/tiket", controllers.api.v1.tiketController.list);
apiRouter.post("/api/v1/tiket", controllers.api.v1.tiketController.create);
apiRouter.put("/api/v1/tiket/:id", controllers.api.v1.tiketController.update);
apiRouter.get("/api/v1/tiket/:id", controllers.api.v1.tiketController.show);
apiRouter.get("/api/v1/tikets", controllers.api.v1.tiketController.findAlllist);
apiRouter.delete(
  "/api/v1/tiket/:id",
  controllers.api.v1.tiketController.destroy
);

// API Pakets Table
apiRouter.get("/api/v1/paket", controllers.api.v1.paketController.list);
apiRouter.post("/api/v1/paket", controllers.api.v1.paketController.create);
apiRouter.put("/api/v1/paket/:id", controllers.api.v1.paketController.update);
apiRouter.get("/api/v1/paket/:id", controllers.api.v1.paketController.show);
apiRouter.get("/api/v1/pakets", controllers.api.v1.paketController.findAlllist);
apiRouter.delete(
  "/api/v1/paket/:id",
  controllers.api.v1.paketController.destroy
);


/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
