'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tiket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Tiket.belongsTo(models.User, {
        foreignKey: "id_user"
      });

      Tiket.belongsTo(models.Paket, {
        foreignKey: "id_paket"
      });
    }
  }
  Tiket.init({
    id_user: DataTypes.INTEGER,
    id_paket: DataTypes.INTEGER,
    no_antrian: DataTypes.INTEGER,
    tanggal: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Tiket',
  });
  return Tiket;
};