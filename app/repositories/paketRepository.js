const { Paket } = require("../models");

module.exports = {
  create(createArgs) {
    return Paket.create(createArgs);
  },

  update(id, updateArgs) {
    return Paket.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return Paket.destroy(id);
  },

  find(id) {
    return Paket.findByPk(id);
  },

  findAll() {
    return Paket.findAll();
  },

  getTotalPaket() {
    return Paket.count();
  },
};
