const { User } = require("../models");

module.exports = {
    create(createArgs){
        return User.create(createArgs);
    },

    getUser({ username }){
        return User.findOne({ where: { username: username } });
    },
}