const { Tiket, Paket,User } = require("../models");

module.exports = {
  create(createArgs) {
    return Tiket.create(createArgs);
  },

  update(id, updateArgs) {
    return Tiket.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return Tiket.destroy(id);
  },

  find(id) {
    return Tiket.findByPk(id,{
      include :[
        {
          model:Paket,
        },
        {
          model:User,
        },
      ],
    });
  },

  findAll() {
    return Tiket.findAll({
      include :[
        {
          model:Paket,
          // as:"Paket",
        },
        {
          model:User,
          // as:"User",
        },
      ],
    });
  },

  getTotalTiket() {
    return Tiket.count();
  },
};
