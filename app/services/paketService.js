const paketRepository = require("../repositories/paketRepository");

module.exports = {
  create(requestBody) {
    return paketRepository.create(requestBody);
  },

  update(id, requestBody) {
    return paketRepository.update(id, requestBody);
  },

  delete(id) {
    return paketRepository.delete(id);
  },

  async list() {
    try {
      const posts = await paketRepository.findAll();
      // const postCount = await paketRepository.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  async findAlllist() {
    try {
      const posts = await paketRepository.findAll();
      // const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return paketRepository.find(id);
  },
};
