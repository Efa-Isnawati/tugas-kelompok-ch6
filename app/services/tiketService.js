const tiketRepository = require("../repositories/tiketRepository");

module.exports = {
  create(requestBody) {
    return tiketRepository.create(requestBody);
  },

  update(id, requestBody) {
    return tiketRepository.update(id, requestBody);
  },

  delete(id) {
    return tiketRepository.delete(id);
  },

  async list() {
    try {
      const posts = await tiketRepository.findAll();
      // const postCount = await tiketRepository.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  async findAlllist() {
    try {
      const posts = await tiketRepository.findAll();
      // const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return tiketRepository.find(id);
  },
};
