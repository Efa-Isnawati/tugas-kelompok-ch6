
const userController = require("./userController");
const paketController = require("./paketController");
const tiketController = require("./tiketController");
const authController = require("./authController")

module.exports = {
    userController,
    tiketController,
    paketController,
    authController,
};
